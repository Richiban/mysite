﻿# @reference ~/Scripts/jQuery-2.0.3.js

window.shared or= {}

class shared.Banner
	init: (@animations = true) ->
		if @animations then setTimeout (=> @startAnimations()), 500

		$("#animations-toggle").click => @toggleAnimations()

	toggleAnimations: ->
		@animations = not @animations

		if @animations then @startAnimations()
		else $('.animated').remove()

	startAnimations: ->
		if @isMobile() then return

		@masthead = $ '#masthead'

		numBoxes = Math.floor Math.random() * 10 + 10

		for i in [1...numBoxes]
			$('<div class="animated-box animated" id="animated-box-' + i + '">')
				.prependTo(@masthead)
				.css( height: @masthead.height() )
				.each (_, element) =>
					box = $ element
					@startingPosition(box)

					moveBox =
						if i % 2 is 0
							@moveRight
						else
							@moveLeft

					moveBox box

	startingPosition: ($box) ->
		randomPlace = Math.floor(Math.random() * @masthead.width())
		randomWidth = Math.floor(Math.random() * @masthead.width() / 10)
		randomOpacity = (Math.random() * 4) / 10

		$box.css
			left: randomPlace
			width: randomWidth
			opacity: 0.5
			'background-position-x': "-#{randomPlace}px"

		#$box.animate backgroundPosition: "(-#{randomPlace}px 0)", 1

	moveLeft: ($box) ->
		randomOpacity = Math.random() * 4 / 10
		randomWidth = Math.floor(Math.random() * @masthead.width() / 10)

		$box.animate(
				left: 0
				width: randomWidth
				'background-position-x': "0"
			,
			@randomTime(),
			=> @moveRight $box
		)

	moveRight: ($box) ->
		randomOpacity = (Math.random() * 4) / 10
		randomWidth = Math.floor(Math.random() * @masthead.width() / 10)
		leftPos = Math.floor(@masthead.width() - randomWidth - 2)

		$box.animate(
				left: leftPos
				width: randomWidth
				'background-position-x': "-" + leftPos + "px"
			,
			@randomTime(),
			=> @moveLeft $box
		)

	randomTime: -> Math.floor(Math.random() * 5000) + 10000

	isMobile: ->
		(/iphone|ipod|android|ie|blackberry|fennec/i).test navigator.userAgent.toLowerCase()