using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace MySite.Web.Host
{
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.Add<StylesheetBundle>("~/Content/Styles");

            bundles.Add<StylesheetBundle>(
                "~/Content/StylesIE",
                bundle => bundle.Condition = "IE");

            bundles.Add<ScriptBundle>("bootstrap", "~/Scripts/bootstrap.js");
            bundles.Add<ScriptBundle>("jquery", "~/Scripts/jquery-2.0.3.js");
            bundles.Add<ScriptBundle>("jquery-ui", "~/Scripts/jquery-ui.js");
            bundles.Add<ScriptBundle>("knockout", "~/Scripts/knockout-3.0.0.js");
            bundles.Add<ScriptBundle>("lazyload", "~/Scripts/lazyload.min.js");
            bundles.Add<ScriptBundle>("modernizr", "~/Scripts/modernizr-2.6.2.js");

            bundles.Add<ScriptBundle>(
                "jquery-bgposition",
                new[] { "~/Scripts/jquery-bgposition.js" },
                bundle => bundle.AddReference("~/jquery"));

            bundles.Add<ScriptBundle>(
                "signalr",
                new[] { "~/Scripts/jquery.signalR-2.0.1.js" },
                bundle => bundle.AddReference("~/jquery"));

            bundles.AddUrlWithAlias<ScriptBundle>(
                "/signalr/hubs",
                "hubs",
                b => b.AddReference("~/signalr"));

            bundles.AddUrlWithAlias<ScriptBundle>(
                "//cdn.jsdelivr.net/angular.all/1.2.19/angular-all.min.js",
                "angular");

            bundles.AddPerIndividualFile<ScriptBundle>("~/Scripts/Shared");

            bundles.Add<ScriptBundle>(
                "~/Scripts/Chat/Index",
                new[] { "~/Scripts/Chat/Index.js" },
                bundle =>
                {
                    bundle.AddReference("~/hubs");
                    bundle.AddReference("~/knockout");
                });
        }
    }
}