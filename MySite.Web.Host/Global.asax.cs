﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MySite.Web.Host.App_Start;

namespace MySite.Web.Host
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var httpConfiguration = GlobalConfiguration.Configuration;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes, httpConfiguration);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ControllerBuilder.Current.SetControllerFactory(typeof(ControllerFactory));
            httpConfiguration.Services.Replace(
                typeof(IHttpControllerActivator),
                new ApiControllerFactory());
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            // Get the exception object.
            var exc = Server.GetLastError();

            // Handle HTTP errors
            if (exc.GetType() == typeof(HttpException))
            {
                // The Complete Error Handling Example generates
                // some errors using URLs with "NoCatch" in them;
                // ignore these here to simulate what would happen
                // if a global.asax handler were not implemented.
                if (exc.Message.Contains("NoCatch") || exc.Message.Contains("maxUrlLength"))
                    return;
            }

            // For other kinds of errors give the user some information
            // but stay on the default page
            Response.Write(String.Format("<h2>Global Page Error</h2><p>{0}</p>\n", exc));
        }
    }
}
