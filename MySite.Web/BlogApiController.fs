﻿namespace MySite.Web.Controllers

open System.Web.Http
open MySite.Contracts
open FSharpx.Task
open MySite.Models
open MySite.Task

[<RoutePrefix "api/blog">]
type BlogApiController
    (
    filterBlogPosts: FilterBlogPostsQuery,
    getBlogPost: GetBlogPostQuery) =

    inherit ApiController()

    let DefaultPageSize = 10

    [<Route "page/{pageNumber:int}"; HttpGet>]
    member this.GetPage pageNumber =
        filterBlogPosts {
            BlogFilterParameters.Default with
                PageSize = Some DefaultPageSize
                PageNumber = pageNumber
        }

    [<Route "{blogPostId}">]
    member this.Get blogPostId =
        task {
            let! blogPostOption = getBlogPost blogPostId

            return
                match blogPostOption with
                | Some post -> post
                | None -> 
                    HttpResponseException System.Net.HttpStatusCode.NotFound
                    |> raise
        }