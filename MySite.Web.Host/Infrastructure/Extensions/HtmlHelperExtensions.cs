﻿using System;
using System.Linq;
using System.Web.Mvc;
using Cassette.Views;
using Newtonsoft.Json;

// ReSharper disable CheckNamespace
namespace MySite.Web.Host.Infrastructure.Extensions
// ReSharper restore CheckNamespace
{
    public static class HtmlHelperExtensions
    {
        public static void AddScriptInitialiser(
            this HtmlHelper @this, string fullJsClassName, params object[] initArgs)
        {
            var jsArgs = String.Join(", ", initArgs.Select(JsonConvert.SerializeObject));

            var variableName = fullJsClassName.Split('.').Reverse().First().ToLower();

            var scriptContents = String.Format(@"
                var {1} = new {0}();
                $(function() {{ {1}.init({2}); }});
            ", fullJsClassName, variableName, jsArgs);

            Bundles.AddInlineScript(scriptContents);
        }

        /// <summary>
        /// Extension for String.Format, returning an MvcHtmlString
        /// </summary>
        /// <param name="formatString"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static MvcHtmlString FMvc(this String formatString, params Object[] args)
        {
            return MvcHtmlString.Create(String.Format(formatString, args));
        }

        public static void RenderPartialAsync<T>(this HtmlHelper<T> html, string partialViewName)
        {
            //html.RenderPartial(partialViewName);
        }

        public static MvcHtmlString RenderPartialAsync<T>(this HtmlHelper<T> html, string partialViewName, object routeValues)
        {
            var strResult = @"<section id='{0}'></section>
<script type='text/javascript'>
    $(function () {
        var link = $('{1}').attr('href') + ' #comments';
        $('#{0}').hide().load(link, function () { $('#{0}').fadeIn('slow'); });
    });
</script>";
            strResult = strResult.Replace("{0}", partialViewName + "-async");
            //strResult = strResult.Replace("{1}", html.ActionLink("Comments", partialViewName, routeValues).ToString());

            return MvcHtmlString.Create(strResult);
        }
    }
}
