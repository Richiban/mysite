﻿module MySite.Web.Tests.Unit.BlogControllerTests

open System
open System.Linq
open System.Threading.Tasks
open System.Web.Mvc

open MySite.Contracts
open MySite.Models
open MySite.Task
open MySite.Web
open MySite.Web.Controllers
open MySite.Web.Models

open Swensen.Unquote.Assertions
open Xunit
open Xunit.Extensions

let emptyPost = {
    Title = ""
    Content = ""
    Link = ""
    Author = ""
    CreatedOn = DateTime.MinValue
    Summary = ""
    Tags = []
    Guid = ""
}

let filterBlogPostsQuery: FilterBlogPostsQuery = fun _ -> task { return Seq.singleton emptyPost }
let getBlogPostQuery: GetBlogPostQuery = fun _ -> task { return None }
let getAllBlogPostsQuery: GetAllBlogPostsQuery = fun _ -> task { return Seq.singleton emptyPost }

let getController() =
    new BlogController(filterBlogPostsQuery, getBlogPostQuery, getAllBlogPostsQuery)

[<Fact>]
let ``Index view result is not null and model properties are correctly set``() =
    let viewResult = getController().Index().Result :?> ViewResult
        
    test <@ viewResult <> null && viewResult.Model <> null @>

    let model = viewResult.Model :?> WordpressPost seq

    test <@ model.Contains emptyPost @>

[<Fact>]
let ``List view result is not null and model properties are correctly set``() =
    let viewResult = getController().List(None, None, None).Result

    let model = viewResult.Model :?> WordpressPost seq

    test <@ model.SequenceEqual [emptyPost] @>

[<Fact>]
let ``List called with no tags returns a redirect result`` () =
    let actionResult = (getController().Tag "").Result
    let redirectResult = actionResult :?> RedirectToRouteResult

    test <@ redirectResult.RouteValues.["action"] :?> string = "Index" @>

[<Fact>]
let ``List called with some tags and some posts returned returns a view result`` () =
    let post = { emptyPost with Guid = Guid.NewGuid().ToString() }
    let filterBlogPostsQuery _ = task { return Seq.singleton post }

    let controller =
        new BlogController(filterBlogPostsQuery, getBlogPostQuery, getAllBlogPostsQuery)

    let actionResult = (controller.Tag "test").Result
    let viewResult = actionResult :?> ViewResult
    let model = viewResult.Model :?> WordpressPost seq

    test <@ model.SequenceEqual [post] @>

[<Fact>]
let ``List called with some tags and no posts returned returns a view result`` () =
    let filterBlogPostsQuery _ = task { return Seq.empty }

    let controller =
        new BlogController(filterBlogPostsQuery, getBlogPostQuery, getAllBlogPostsQuery)
        
    let viewResult = (controller.Tag "test").Result :?> ViewResult
    let model = viewResult.Model :?> WordpressPost seq

    test <@ model.Any() = false @>

[<Fact>]
let ``List called with some tags and no posts returned sets a notification`` () =
    let filterBlogPostsQuery _ = task { return Seq.empty }

    let controller =
        new BlogController(filterBlogPostsQuery, getBlogPostQuery, getAllBlogPostsQuery)

    let viewResult = (controller.Tag "test").Result :?> ViewResult
    let notification = viewResult.ViewData.["Notification"] :?> string

    test <@ notification = "No results for posts tagged \"test\"" @>

[<Fact>]
let ``Read called and post found returns viewResult with model set`` () =
    let getBlogPostQuery _ = task { return Some emptyPost }

    let controller =
        new BlogController(filterBlogPostsQuery, getBlogPostQuery, getAllBlogPostsQuery)

    let viewResult = controller.Read("").Result :?> ViewResult

    let model = viewResult.Model :?> WordpressPost

    test <@ model = emptyPost @>

[<Fact>]
let ``Read called and post not found returns status result 404`` () =
    let statusResult = getController().Read("").Result :?> HttpStatusCodeResult

    let status = statusResult.StatusCode

    test <@ status = 404 @>