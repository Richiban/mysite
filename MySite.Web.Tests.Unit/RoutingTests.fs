﻿module RoutingTests

open System
open System.Web.Routing
open System.Web.Mvc
open System.Web.Mvc.Routing

open MySite.Web.Controllers

open Xunit
open Xunit.Extensions

[<Theory>]
[<InlineData(typeof<HomeController>, "~/BaseMethodWithRouteTypo")>]
let ``Attribute routing with inheritance; invalid paths`` (derivedController : Type, path : string) = ()
//    // Arrange
//    let controllerTypes = [derivedController; derivedController.BaseType]
//    let routes = new RouteCollection()
//    routes.MapMvcAttributeRoutes(controllerTypes)
//    HttpContextBase context = GetContext(path)
//
//    // Act
//    RouteData routeData = routes.GetRouteData(context)
//
//    // Assert
//    Assert.Null(routeData)