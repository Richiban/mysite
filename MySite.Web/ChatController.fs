﻿namespace MySite.Web.Controllers

open System.Web.Mvc

[<RoutePrefix "chat">]
type ChatController () =
    inherit MySiteController ()

    [<Route "">]
    member this.Index () = base.View()