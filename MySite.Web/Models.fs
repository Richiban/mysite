﻿namespace MySite.Web.Models

open System
open System.Collections.Generic
open System.ComponentModel
open System.ComponentModel.DataAnnotations
open MySite.Models

[<CLIMutable>]
type HomeViewModel = {
    [<DisplayName "Blog"; Description "Here is some of the latest from my WordPress blog">]
    BlogUpdates : BlogSummary seq

    [<DisplayName "Photography">]
    PhotographyUpdates : IDictionary<int, String>

    [<DisplayName "Music">]
    MusicUpdates :  IDictionary<int, String>

    Age : int

    Location : string

    Employer : string

    Profession : string

    SiteName : string

    [<DataType(DataType.EmailAddress)>]
    EmailAddress : string
}