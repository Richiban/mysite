﻿module MySite.Core.Tests.Unit.TranslationTests

open System
open System.Linq
open System.Threading.Tasks

open MySite.Contracts
open MySite.Models
open MySite.Task
open MySite.Core.Translations

open Swensen.Unquote.Assertions
open Xunit
open Xunit.Extensions

[<Theory>]
[<InlineData ("Hello world", 5, "Hello", " world")>]
[<InlineData ("Hello world", 0, "", "Hello world")>]
[<InlineData ("Hello world", 11, "Hello world", "")>]
let ``splitOnIndex works on valid inputs`` (str, index, expectedLeft, expectedRight) =
    let left, right = splitOnIndex index str

    test <@ left = expectedLeft && right = expectedRight @>

[<Theory>]
[<InlineData ("Hello world", 12)>]
[<InlineData ("Hello world", -1)>]
let ``splitOnIndex fails on out-of-range index`` (str, index) =
    raises<ArgumentOutOfRangeException> <@ splitOnIndex index str @>

[<Theory>]
[<InlineData ("Hi.", 1, "Hi.")>]
[<InlineData ("Hi.", 2, "Hi.")>]
[<InlineData ("Hi. My name is Bob. What's yours?", 2, "Hi. My name is Bob. ")>]
[<InlineData ("Hi. My name is Bob. What's yours?", 3, "Hi. My name is Bob. What's yours?")>]
[<InlineData (" ", 1, " ")>]
[<InlineData ("Hello? Is that you? Good! Thanks.", 3, "Hello? Is that you? Good! ")>]
[<InlineData ("<p>Sentence one</p><p>Sentence two</p>", 1, "<p>Sentence one</p>")>]
let ``FirstNSentences works correctly`` (str, n, expectedResult) =
    test <@ firstNSentences n str = expectedResult @>

[<Fact>] let test1 () = ()