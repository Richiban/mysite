# @reference ~/Scripts/jQuery-2.0.3.js

window.shared or= {}

class shared.Layout
	loadAjaxLink: ->
		href = location.href.split("#")[1]

		@addLoadingLayer()

		#$('.page').load href + " .page", => @removeLoadingLayer()

		$.ajax
			url: href
			success: (data) =>
				@removeLoadingLayer()

				dataHtml = $(data).find(".page").html()

				$('.page').html dataHtml

				@setupAjaxLinks()

			error: @handleAjaxError

	handleAjaxError: (jqXHR, textStatus, errorThrown) ->
		console?.log "Error loading page via ajax: #{errorThrown}"

	addLoadingLayer: ->
		$(".page").fadeOut()

	removeLoadingLayer: ->
		$(".page").stop().fadeIn()

	setupAjaxLinks: ->
		if @ajaxPageLoads
			console?.log "Setting up ajax links"

			$('a:not(.no-ajax)').each ->
				$this = $ @
				href = $this.attr 'href'

				canBeAnchored = (link) ->
					disallowed = ["http", "mailto:", "#", "/#"]

					for item in disallowed
						if link.startsWith item then return false

					true

				if canBeAnchored href
					anchorHref = "#" + href
					$this.attr("href", anchorHref)

			window.onhashchange = => @loadAjaxLink location.href

		else
			console?.log "Not setting up ajax links"

	#	Page start function
	init: (@ajaxPageLoads = true, animations = true) ->
		_this = this

		if typeof String.prototype.startsWith != 'function'
			String.prototype.startsWith = (str) -> @indexOf(str) == 0

		if @ajaxPageLoads
			@setupAjaxLinks()

			#	If an ajax link is given when the page loads, load that link
			
			if location.href.indexOf('#') != -1
				link = location.href.split('#')[1]
				@loadAjaxLink(link)