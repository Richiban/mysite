﻿namespace MySite.Web

open System
open System.Collections.Generic
open System.Threading.Tasks
open System.Net
open System.Web.Http
open System.Web.Http.Controllers
open System.Web.Http.Dispatcher
open System.Web.Mvc

open MySite
open MySite.Contracts
open MySite.Models
open MySite.Queries.Caching
open MySite.Queries.Wordpress
open MySite.Task
open MySite.Web.Controllers

type UnconfiguredControllerException(controllerName : String) as this =
    inherit HttpResponseException(HttpStatusCode.NotFound)

    do this.Data.["AttemptedController"] <- controllerName

module DI =
    let wordpressUrl = "http://richiban.wordpress.com/feed/"
    let cache' a = cache (new CacheStore()) a
    let getFeedXml = cache' (GetFeedXml wordpressUrl)
    let getAllPosts = cache' (GetAllPosts getFeedXml)
    let getBlogSummariesQuery = cache' (GetBlogSummaries getAllPosts)
    let filterPosts = filterBlogPosts getAllPosts
    let getBlogPost = GetBlogPost getAllPosts
    
open DI

type ControllerFactory () =

    inherit DefaultControllerFactory()

    override this.CreateController (requestContext, controllerName) =
        match controllerName with
        | "Home" -> new HomeController (getBlogSummariesQuery) :> IController
        | "Blog" -> new BlogController (filterPosts, getBlogPost, getAllPosts) :> IController
        | "Chat" -> new ChatController() :> IController
        | _ -> raise (UnconfiguredControllerException controllerName)

    override this.ReleaseController controller =
        match controller with
        | :? IDisposable as disposable -> disposable.Dispose()
        | _ -> ()

type ApiControllerFactory() =
    interface IHttpControllerActivator with
        member this.Create (request, controllerDescriptor, controllerType) =

            match controllerDescriptor.ControllerName with
            | "BlogApi" -> 
                new BlogApiController(filterPosts, getBlogPost) :> IHttpController
            | unrecognisedControllerName ->
                raise (UnconfiguredControllerException unrecognisedControllerName)