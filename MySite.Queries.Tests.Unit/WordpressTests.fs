﻿namespace MySite.Queries.Tests.Unit

open System
open System.Xml

open MySite
open MySite.Models
open MySite.Task

open Swensen.Unquote.Assertions
open Xunit
open Xunit.Extensions

module ``Wordpress Tests`` =

    [<Fact>] 
    let ``Test that getting feed xml with an empty Wordpress URL throws exception`` () =
        let getXmlContent = Queries.Wordpress.GetFeedXml

        raises<ArgumentNullException> <@ getXmlContent "" @>

module ``Compare Tests`` =
    let emptyPost = {
        Title = ""
        Content = ""
        Link = ""
        Author = ""
        CreatedOn = DateTime.MinValue
        Summary = ""
        Tags = []
        Guid = ""
    }

    [<Fact>]
    let ``Compare with year on filter and no year on post returns false`` () =
        let filter = { BlogFilterParameters.Default with Year = Some 2005 }
        let post = emptyPost

        let actual = Queries.Wordpress.compare filter post

        test <@ actual = false @>

    [<Fact>]
    let ``Compare with no year on filter and no year on post returns true`` () =
        let filter = BlogFilterParameters.Default
        let post = emptyPost

        let actual = Queries.Wordpress.compare filter post

        test <@ actual = true @>

    [<Fact>]
    let ``Compare with no year on filter and year on post returns true`` () =
        let filter = BlogFilterParameters.Default
        let post = { emptyPost with CreatedOn = (DateTime(2001, 1, 1)) }

        let actual = Queries.Wordpress.compare filter post

        test <@ actual = true @>

    [<Fact>]
    let ``Compare with year on filter and same year on post returns true`` () =
        let filter = { BlogFilterParameters.Default with Year = Some 2001 }
        let post = { emptyPost with CreatedOn = (DateTime(2001, 1, 1)) }

        let actual = Queries.Wordpress.compare filter post

        test <@ actual = true @>

    [<Fact>]
    let ``Compare with year on filter and different year on post returns false`` () =
        let filter = { BlogFilterParameters.Default with Year = Some 2000 }
        let post = { emptyPost with CreatedOn = (DateTime(2001, 1, 1)) }

        let actual = Queries.Wordpress.compare filter post

        test <@ actual = false @>

    [<Fact>]
    let ``Compare with no month on filter and month on post returns true`` () =
        let filter = BlogFilterParameters.Default
        let post = { emptyPost with CreatedOn = (DateTime(1, 1, 1)) }

        let actual = Queries.Wordpress.compare filter post

        test <@ actual = true @>

    [<Fact>]
    let ``Compare with no month on filter and no month on post returns true`` () =
        let filter = BlogFilterParameters.Default
        let post = emptyPost

        let actual = Queries.Wordpress.compare filter post

        test <@ actual = true @>
