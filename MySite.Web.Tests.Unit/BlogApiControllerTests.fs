﻿module MySite.Web.Tests.Unit.BlogApiControllerTests

open System
open System.Linq
open System.Net
open System.Threading.Tasks
open System.Web.Mvc
open System.Web.Http

open MySite.Contracts
open MySite.Models
open MySite.Task
open MySite.Web
open MySite.Web.Controllers
open MySite.Web.Models

open Swensen.Unquote.Assertions
open Xunit
open Xunit.Extensions

let defaultPost = {
    Title = ""
    Content = ""
    Link = ""
    Author = ""
    CreatedOn = DateTime.MinValue
    Summary = ""
    Tags = []
    Guid = ""
}

let (|WrappedHttpNotFoundException|_|) (e : Exception) =
    match e with
    | :? AggregateException as e' ->
        match e'.InnerExceptions.Single() with
        | :? HttpResponseException as e' when e'.Response.StatusCode = HttpStatusCode.NotFound -> Some()
        | _ -> None
    | _ -> None

[<Fact>]
let ``Get returns the found blog post``() =
    let guid = Guid.NewGuid().ToString()

    let filterBlogPosts _ = task { return Seq.empty }
    let getBlogPost id = task { return Some { defaultPost with Guid = guid } }
    let controller = new BlogApiController(filterBlogPosts, getBlogPost)

    let result = 
        controller.Get(guid).Result

    test <@ result.Guid = guid @>

[<Fact>]
let ``Exception is called on Get when no post is found``() =
    let filterBlogPosts _ = task { return Seq.empty }
    let getBlogPost id = task { return None }
    let controller = new BlogApiController(filterBlogPosts, getBlogPost)

    try
        controller.Get("").Result |> ignore
        failwith "No exception thrown"
    with
        | WrappedHttpNotFoundException -> ()
        | e ->
            failwithf "the wrong type of exception was thrown: %A" e

    ()

[<Fact>]
let ``GetPage returns blog posts`` () =
    let post = { defaultPost with Guid = Guid.NewGuid().ToString() }
    let filterBlogPosts _ = task { return Seq.singleton post }
    let getBlogPost id = task { return None }
    let controller = new BlogApiController(filterBlogPosts, getBlogPost)

    let result = (controller.GetPage 0).Result

    test <@ result.Single() = post @>