﻿module MySite.Models

open System.ComponentModel.DataAnnotations

type BlogFilterParameters = {
    Year: int option

    Month: int option

    Day: int option

    Tags: string seq

    PageSize: int option

    PageNumber: int
} with 
    static member Default = {
        Year = None
        Month = None
        Day = None
        Tags = []
        PageSize = None
        PageNumber = 0
    }

type BlogSummary = {
    Title: string

    [<DataType(DataType.Html)>]
    SummaryText: string

    Guid: string
}

open System

type WordpressPost = {
    Title: string

    [<DataType(DataType.Html)>]
    Content: string

    Link: string
    Author: string
    CreatedOn: DateTime
    Summary: string
    Tags: string seq
    Guid: string
}