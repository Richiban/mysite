﻿module MySite.Extensions

open System
type DateTime with 
    member dateTo.YearsSince (dateFrom : DateTime) =
        let yearDiff = dateTo.Year - dateFrom.Year

        if not(dateFrom.AddYears(yearDiff) <= dateTo)
        then yearDiff - 1
        else yearDiff

    static member tryParse input =
        match DateTime.TryParse input with
        | true, date -> Some date
        | _ -> None