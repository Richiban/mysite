﻿namespace MySite.Queries

open System
open System.Net
open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open System.Xml.Linq
open GibRAD
open MySite.Models
open MySite.Extensions
open FSharpx.Task
open MySite.Task
open MySite.Contracts
open MySite.Core.Translations

module Wordpress = 
    let (|NonEmpty|) = 
        function 
        | "" -> nullArg "s"
        | s -> s
    
    let GetFeedXml(NonEmpty wordpressUrl) : GetWordpressXmlQuery = 
        fun () -> 
            use wc = new WebClient()
            wc.DownloadStringTaskAsync(Uri wordpressUrl)
    
    let GetBlogSummaries(getAllPosts : GetAllBlogPostsQuery) : GetBlogSummariesQuery = 
        fun () -> task { let! allPosts = getAllPosts()
                         return allPosts |> Seq.map (WordpressPostToBlogSummary) }
    
    let GetAllPosts (getXml : GetWordpressXmlQuery) : GetAllBlogPostsQuery =
        fun () -> 
            task {
                let! xmlContents = getXml()
                let xml = XDocument.Parse xmlContents
                let contentNamespace = XNamespace.Get "http://purl.org/rss/1.0/modules/content/"
                let dcNamespace = XNamespace.Get "http://purl.org/dc/elements/1.1/"

                let inline getValue (parent : XElement) childName =
                    try
                        (parent.Element (XName.Get childName)).Value
                    with
                    | e -> raise (new Exception ("Could not get value of XML element: " + childName))

                let inline getValueX (parent : XElement) xChild =
                    (parent.Element xChild).Value

                let inline getElementValues (parent : XElement) childName = parent.Elements <| XName.Get childName
                                                                            |> Seq.map (fun it -> it.Value)
                return seq { 
                           for item in xml.Descendants("item" |> XName.Get) -> 
                               let getValue = getValue item
                               let getValueX = getValueX item
                               let getValues = getElementValues item
                               { Title = getValue "title"
                                 Link = getValue "link"
                                 Content = getValueX (contentNamespace + "encoded")
                                 Tags = getValues "category"
                                 Author =  getValueX (dcNamespace + "creator")
                                 Summary = getValue ("description")
                                 CreatedOn = getValue ("pubDate") |> DateTime.Parse
                                 Guid = (Uri(getValue ("guid"))).GetQueryItem("p") }
                       }
            }
    
    let GetBlogPost (allPostsQuery : GetAllBlogPostsQuery) : GetBlogPostQuery =
        fun id ->
            task { let! allPosts = allPostsQuery()
                   return allPosts |> Seq.tryFind (fun x -> x.Guid = id) }
    
    let compare filterParams wordpressPost = 
        let (|Date|) (d : DateTime) = 
            (d.Year, d.Month, d.Day)
        
        let compareYear filterParams wordpressPost = 
            match filterParams.Year, wordpressPost.CreatedOn with
            | None, _ -> true
            | Some leftYear, Date(rightYear, _, _) -> rightYear = leftYear
            | _ -> false
        
        let compareMonth filterParams wordpressPost = 
            match filterParams.Month, wordpressPost.CreatedOn with
            | None, _ -> true
            | Some leftMonth, Date(_, rightMonth, _) -> rightMonth = leftMonth
            | _ -> false
        
        let compareDay filterParams wordpressPost = 
            match filterParams.Day, wordpressPost.CreatedOn with
            | None, _ -> true
            | Some leftDay, Date(_, _, rightDay) -> rightDay = leftDay
            | _ -> false
        
        let rules = [ compareYear ]
        rules |> Seq.forall (fun rule -> (rule filterParams wordpressPost) = true)
    
    let filterBlogPosts (allPostsQuery : GetAllBlogPostsQuery) : FilterBlogPostsQuery =
        fun (p : BlogFilterParameters) ->
            task { let! allPosts = allPostsQuery()
                   return Seq.filter (compare p) allPosts }
