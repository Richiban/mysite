﻿module CachingTests

open System
open System.Threading

open MySite.Queries.Caching

open Swensen.Unquote
open Xunit
open Xunit.Extensions

let setup () =
    let callCount = ref 0
    let sampleFunc () = callCount := !callCount + 1; !callCount
    sampleFunc

[<Fact>]
let ``Calling the sample function results in a different return value each call ``() =
    let sampleFunc = setup()
    let call1 = sampleFunc ()
    let call2 = sampleFunc ()

    test <@ call1 = 1 && call2 = 2 @>

[<Fact>]
let ``Caching a function causes that function to return the same result when called for a second time``() =
    let sampleFunc = setup()
    let cacheStore = CacheStore()
    let cachedF = cache cacheStore sampleFunc

    let call1 = cachedF ()
    let call2 = cachedF ()

    test <@ call1 = 1 && call2 = 1 @>

[<Fact>]
let ``Caching a function and allowing the cached value to expire causes the function to be rerun when requested again``() =
    let sampleFunc = setup()
    let cacheStore = CacheStore(TimeSpan.FromTicks 1L)
    let cachedF = cache cacheStore sampleFunc

    let call1 = cachedF ()
    Thread.Sleep(TimeSpan.FromMilliseconds 1.0)
    let call2 = cachedF ()

    test <@ call1 = 1 && call2 = 2 @>

[<Fact>]
let ``Caching a function and changing the type of the cached value causes the function to be rerun``() =
    let sampleFunc = setup()
    let cacheStore = CacheStore()
    let cachedF = cache cacheStore sampleFunc

    let call1 = cachedF ()

    let cachedItem = cacheStore |> Seq.head
    cacheStore.[cachedItem.Key] <- {
         expiry = cachedItem.Value.expiry
         value = obj()
    }

    let call2 = cachedF ()

    test <@ call1 = 1 && call2 = 2 @>