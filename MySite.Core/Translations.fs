﻿module MySite.Core.Translations

open System
open System.Linq

open MySite.Models

let splitOnIndex index (input : string) =
    input.Substring(0, index), input.Substring index

let rec splitSentences (input : string) =
    let punctuation = [". "; "! "; "? "; "</p>"]

    seq {
        let foundPunctuation = 
            punctuation 
            |> List.choose (fun mark ->
                let index = input.IndexOf mark
                if index >= 0 then Some (mark, index) else None)

        match foundPunctuation with
        | [] -> yield input 
        | list ->
            let mark, markIndex = list |> List.minBy snd
            let splitIndex = markIndex + mark.Length
            let firstPart, secondPart = splitOnIndex splitIndex input
            yield firstPart
            yield! splitSentences secondPart
    }

let firstNSentences n s = (splitSentences s).Take n |> String.concat ""

let WordpressPostToBlogSummary post = {
    Title = post.Title
    SummaryText = post.Content |> firstNSentences 2
    Guid = post.Guid
}