﻿module MySite.Contracts

open System.Threading.Tasks
open MySite.Models

type FilterBlogPostsQuery = BlogFilterParameters -> WordpressPost seq Task

type GetBlogPostQuery = string -> WordpressPost option Task

type GetAllBlogPostsQuery = unit -> WordpressPost seq Task

type GetBlogSummariesQuery = unit -> BlogSummary seq Task

type GetWordpressXmlQuery = unit -> string Task