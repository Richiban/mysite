﻿module MySite.Web.Tests.Unit.HomeControllerTests

open System.Linq
open System.Threading.Tasks
open System.Web.Mvc

open MySite.Contracts
open MySite.Models
open MySite.Task
open MySite.Web
open MySite.Web.Controllers
open MySite.Web.Models

open Swensen.Unquote.Assertions
open Xunit
open Xunit.Extensions

let optionToSeq arg =
    match arg with
    | None -> Seq.empty
    | Some value -> Seq.singleton value

let makeController blogSummary =
    let blogSummarySeq = optionToSeq blogSummary
    
    new HomeController(fun () -> task { return blogSummarySeq })

[<Fact>]
let ``Index view result is not null and model properties are correctly set``() =
    let homeController = makeController None
    let viewResult = homeController.Index().Result :?> ViewResult

    test <@ viewResult <> null @>

    let model = viewResult.Model :?> HomeViewModel

    test <@ model.Age = 29 @>
    test <@ model.Employer = "Asos" @>
    test <@ model.Location = "London" @>
    test <@ model.Profession = "Developer" @>
    test <@ model.SiteName = "gibson.cx" @>
    test <@ model.BlogUpdates.Any() = false @>

let defaultSummary = {
    Title = ""
    SummaryText = ""
    Guid = ""
}

[<Fact>]
let ``Index view result is not null and contains one blog update``() =
    let homeController = makeController (Some defaultSummary)
    let viewResult = homeController.Index().Result :?> ViewResult

    test <@ viewResult <> null @>

    let model = viewResult.Model :?> HomeViewModel

    test <@ model.BlogUpdates.Any() @>

[<Fact>]
let ``About view result is not null``() =
    let homeController = makeController None
    let viewResult = homeController.About()
            
    test <@ viewResult <> null @>