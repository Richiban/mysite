﻿namespace MySite.Queries

open System.Net

module Twitter =
    type GetTwitterXmlQuery = unit -> string

    let TwitterUrl = "https://twitter.com/mesonofgib"

    let GetTwitterXml twitterUrl () =
        async {
            let! twitterXml = (new WebClient()).AsyncDownloadString twitterUrl

            return twitterXml
        }