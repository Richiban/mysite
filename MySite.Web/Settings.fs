﻿module MySite.Web.Settings

open System

type Settings = {
    Location : string
    Profession : string
    Employer : string
    SiteName : string
    EmailAddress : string
    DateOfBirth : DateTime
}

let Default = {
    Location = "London"
    Profession = "Developer"
    Employer = "Asos"
    SiteName = "gibson.cx"
    EmailAddress = "richard@gibson.cx"   
    DateOfBirth = DateTime(1985, 04, 16)
}