﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace MySite.Web.Host.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes, HttpConfiguration config)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();
            //config.MapHttpAttributeRoutes();
        }
    }
}
