﻿namespace MySite.Web.Controllers

open System
open System.Collections.Generic
open System.Linq
open System.Net
open System.Threading.Tasks
open System.Web.Mvc

open MySite.Contracts
open MySite.Models
open MySite.Queries
open MySite.Task
open MySite.Web.Models

[<RoutePrefix "blog">]
type BlogController
    (
    filterBlogPosts: FilterBlogPostsQuery,
    getBlogPost: GetBlogPostQuery,
    getAllBlogPosts: GetAllBlogPostsQuery) =

    inherit MySiteController()
        
    [<Route "">]
    member this.Index() =
        task {
            let! posts = getAllBlogPosts ()

            return this.View posts
        }
        

    [<Route "{year:int}/{month}/{day}">]
    member this.List(year, month, day) =
        task {
            let! posts = 
                filterBlogPosts {
                    BlogFilterParameters.Default with
                        Year = year
                        Month = month
                        Day = day
                }

            return this.View("Index", posts)
        }
         
    member this.RedirectToAction actionName = base.RedirectToAction actionName

    [<Route "tag/{*tags}">]
    member this.Tag tags =
        task {
            if String.IsNullOrEmpty tags
            then return this.RedirectToAction("Index") :> ActionResult
            else
                let splitTags = tags.Split '/'

                let blogFilterParameters = {
                    BlogFilterParameters.Default with
                        Tags = splitTags
                }
                 
                let! posts = filterBlogPosts blogFilterParameters

                if not <| posts.Any()
                then
                    let message = sprintf "No results for posts tagged \"%s\"" tags
                    this.SetNotification message

                return this.View posts
        }

    [<Route "read/{blogId}">]
    member this.Read blogId =
        task {
            let! blogPostOption = getBlogPost blogId

            return
                match blogPostOption with
                | None -> HttpStatusCodeResult 404 :> ActionResult
                | Some post -> this.View post
        }