﻿module MySite.Queries.Caching

open System
open System.Collections.Concurrent

type CacheKey = { functionId: Guid; argument: obj }
type CacheValue = { expiry: DateTime; value: obj }

type CacheStore(?defaultExpiry : TimeSpan) =
    inherit ConcurrentDictionary<CacheKey, CacheValue>()

    let defaultExpiry = defaultArg defaultExpiry (TimeSpan.FromSeconds 100.0)

    member this.AddWithDefaultExpiry key value =
        this.AddWithCustomExpiry defaultExpiry key value

    member this.AddWithCustomExpiry expiryTime key value =
        this.[key] <- {
            expiry = DateTime.Now + expiryTime
            value = value
        }

/// Wraps the given function in another function that caches the original function's
/// result and will, on subsequent calls, return the cached result. 
/// Recommend using this only for functions whose arguments have custom equality, or
/// you may not see much benefit.
let cache<'a, 'b> (store : CacheStore) (f : 'a -> 'b) : 'a -> 'b=
    let functionGuid = System.Guid.NewGuid()

    fun arg ->
        let cacheKey = { functionId = functionGuid; argument = arg }

        let (|Contains|Expired|NotPresent|Incorrect|) (cacheStore, cacheKey) =        
            if store.ContainsKey cacheKey
            then
                let {expiry = expiry; value = value} = store.[cacheKey]
                
                match value with
                | :? 'b as value' -> 
                    if expiry < DateTime.Now
                    then Expired (DateTime.Now - expiry)
                    else Contains value'
                | _ ->
                    Incorrect
            else
                NotPresent

        let reAdd () =
            let newValue = f arg
            store.AddWithDefaultExpiry cacheKey (newValue :> obj)
            newValue

        match store, cacheKey with
        | Contains value -> value
        | _ -> reAdd ()