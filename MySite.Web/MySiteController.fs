﻿namespace MySite.Web.Controllers

open System.Web.Mvc

[<AbstractClass>]
type MySiteController() = 
    inherit Controller()
    
    member this.SetNotification (message : string) =
        base.ViewData.Add("Notification", message)
    
    member this.View(arg: obj) = base.View arg :> ActionResult
    member this.View(viewName : string, model : obj) = base.View (viewName, model)
    