﻿namespace MySite.Web.Controllers

open System
open System.Web.Mvc

open MySite.Extensions
open MySite.Contracts
open MySite.Task
open MySite.Web.Models

open FSharpx.Task

[<RoutePrefix "">]
type HomeController (getBlogSummaries : GetBlogSummariesQuery) =
    inherit MySiteController()
    
    [<Route "">]
    member this.Index() =
        task {
            let! summaries = getBlogSummaries ()

            let settings = MySite.Web.Settings.Default

            let homeViewModel = {
                BlogUpdates = summaries
                Location = settings.Location
                Profession = settings.Profession
                Employer = settings.Employer
                SiteName = settings.SiteName
                Age = DateTime.Now.YearsSince( settings.DateOfBirth )
                EmailAddress = settings.EmailAddress
                MusicUpdates = dict[]
                PhotographyUpdates = dict[]
            }

            return this.View homeViewModel
        }

    [<Route "about">]
    member this.About() = base.View()