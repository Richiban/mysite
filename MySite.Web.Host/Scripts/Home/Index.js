var MySite;
(function (MySite) {
    /// <reference path="~/Scripts/knockout-3.0.0.js"/>
    (function (Chat) {
        var ChatMessage = (function () {
            function ChatMessage(author, messageText, createdOn) {
                this.author = author;
                this.messageText = messageText;
                this.createdOn = createdOn;
            }
            return ChatMessage;
        })();
        Chat.ChatMessage = ChatMessage;

        var ChatUser = (function () {
            function ChatUser(name, alias) {
                this.name = name;
            }
            return ChatUser;
        })();
        Chat.ChatUser = ChatUser;

        var ChatSession = (function () {
            function ChatSession(user, startedOn) {
                this.user = user;
                this.startedOn = startedOn;
            }
            return ChatSession;
        })();
        Chat.ChatSession = ChatSession;

        var ChatRoom = (function () {
            function ChatRoom() {
                this.messages = [];
                this.sessions = [];
            }
            ChatRoom.prototype.addMessage = function (newMessage) {
                this.messages.push(newMessage);
            };

            ChatRoom.prototype.connectSession = function (newSession) {
                this.sessions.push(newSession);
            };
            return ChatRoom;
        })();
        Chat.ChatRoom = ChatRoom;

        // Overall viewmodel for this screen, along with initial state
        var ChatViewModel = (function () {
            function ChatViewModel() {
                // Editable data
                this.chatMessages = ko.observableArray([
                    new ChatMessage("Steve", "Hello?", new Date().toTimeString()),
                    new ChatMessage("Bert", "Oh, hey Steve", "One minute ago")
                ]);
                this.addMessage = function (newMessage) {
                    this.chatMessages.push(newMessage);
                };
                this.newMessageFromPage = ko.observable(new ChatMessage(null, null, null));
            }
            return ChatViewModel;
        })();
        Chat.ChatViewModel = ChatViewModel;

        var Index = (function () {
            function Index() {
                this.chatVM = new ChatViewModel();
            }
            Index.prototype.init = function () {
                this.chatRoom = new ChatRoom();
                ko.applyBindings(this.chatVM);

                this.startSignalR();
            };

            Index.prototype.startSignalR = function () {
                var _this = this;
                var connection = $.connection;
                var chat = connection.chatHub;

                chat.client.addNewMessageToPage = function (newMessage) {
                    _this.chatVM.addMessage(newMessage);
                };

                $.connection.hub.start().done(function () {
                    _this.chatVM.sendMessage = function () {
                        var newMessage = _this.chatVM.newMessageFromPage();

                        chat.server.send(newMessage);

                        _this.chatVM.newMessageFromPage(new ChatMessage("", "", ""));
                    };
                });
            };
            return Index;
        })();
        Chat.Index = Index;
    })(MySite.Chat || (MySite.Chat = {}));
    var Chat = MySite.Chat;
})(MySite || (MySite = {}));
